const webpack = require('webpack');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
module.exports = {
  webpack: {
    configure: {
      resolve: {
        fallback: {
          http: require.resolve('stream-http'),
          https: require.resolve('https-browserify'),
          os: require.resolve('os-browserify/browser'),
          stream: require.resolve('stream-browserify')
        }
      },
      plugins: [
        // new BundleAnalyzerPlugin({ analyzerMode: 'server' }),
        new webpack.ProvidePlugin({
          Buffer: ['buffer', 'Buffer']
        }),
        new webpack.ProvidePlugin({
          process: 'process/browser'
        })
      ]
    }
  }
};
