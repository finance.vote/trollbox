
## Table of Contents

- [Explanation](#explanation)
- [Install](#install)
- [Testing](#testing)
- [Testnets](#testnets)
- [Contribute](#contribute)
- [License](#license)

## Explanation
This is home to the prediction market webapp and smart contracts. The basic idea here is that users come to the site, create an identity if they don't have one or want another, select an identity to vote with and then vote in one of the various prediction markets available (called Tournaments in the contract). The voting is done via a quadratic voting system, where a user has a given amount of voice credits per vote and voting multiple times on the same item is permitted but costs a quadratic amount of voice credits. For example, voting once on a given choice costs 1 voice credit, voting twice on it costs a total of 4 voice credits, and voting 3 times costs 9 voice credits. The amount of voice credits available is set at tournament creation time, and tournaments can only be created by a privileged management address. Each identity is allowed to vote once per round in the tournament. A tournament also has a round length, which determines when the voting closes. Tournament winners are resolved one round length after voting closes, ie when the next round's votes close. Each identity may vote once per round. Users with multiple identities are not restricted from voting multiple times, once with each identity. That's part of the game, pay-to-sybil. After each round's winners are determined, users are awarded an amount of tokens. The total round reward is determined at tournament-creation time, and this reward is distributed across users who voted for the winning choice, proportional to the number of votes (not voice credits) that they allocated to that choice. Users can accumulate token balances independent of the token balance of the contract, and they can withdraw them at will, subject to the token balance of the contract. Rounds are resolved by the winnerOracle address, which selects a winner out of the options. Choices are determined by a token list supplied at an ENS address determined at tournament-creation time, so the winnerOracle can simply supply an index into that list as a winningOption. In addition to winning tokens for choosing the winning choice, identities are rewarded with additional voice credits in the next round, the amount of which is the square of the number of times they voted on the winning choice. Identities have metadata associated in the trollbox contract that keeps track of their additional voice credits as well as a rank. Their rank is updated via a rankManager address, which is not implemented and will initially be a privileged key, since we have not completed the design of the mechanic yet. Tournaments are gated by rank, meaning that tournaments may be created that a user can only vote in if they have at least the required rank. 

## Testnets
The official ropsten addresses are:
- SafeMathLib: [0x2365A147D3fd5522e375e1c17eeC780095b73d49](https://ropsten.etherscan.io/address/0x2365A147D3fd5522e375e1c17eeC780095b73d49)
- Token: [0x3a7e9F41C1d28F87Fab37D557529e8c4e6e4a9F0](https://ropsten.etherscan.io/token/0x3a7e9F41C1d28F87Fab37D557529e8c4e6e4a9F0)
- BondingCurve: [0x2f0dAe36Fe1b00AcaC4A73bBb1871648a4053CC6](https://ropsten.etherscan.io/address/0x2f0dAe36Fe1b00AcaC4A73bBb1871648a4053CC6)


## Install 

``` bash
# install dependencies
yarn
# deploy contracts to local ganache in a docker and start storybook
yarn dev
# build webapp and deploy to ipfs
yarn build:ipfs

```

## Testing


## Contribute

To report bugs within this package, create an issue in this repository.
For security issues, please contact chris@finance.vote
When submitting code ensure that it is free of lint errors and has 100% test coverage.


## License

[GNU General Public License v3.0 (c) 2018 Fragments, Inc.](./LICENSE)

