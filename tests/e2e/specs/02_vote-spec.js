import Vote from "../pages/vote/vote-page";

const vote = new Vote();

describe("Vote tests", () => {
  before(() => {
    vote.visit();
  });
  context("Initial", () => {
    it(`vote button should be disabled`, () => {
      vote.getVoteButton().should("have.attr", "disabled");
    });
    it(`overallocated warning should be shown and vote button should be disabled`, () => {
      vote.setAllVotesValue(4);
      vote.wait(2000);
      vote.getVoteButton().should("have.attr", "disabled");
      vote.getVoteDangerText().should("exist");
    });
  });
});
