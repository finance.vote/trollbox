/* eslint-disable ui-testing/no-hard-wait */
/* eslint-disable cypress/no-unnecessary-waiting */
import Page from "../page";

export default class VotePage extends Page {
  constructor() {
    super();
  }

  getVoteButton() {
    return cy.get(".vote-form__submit-button");
  }

  visit() {
    cy.visit("/#/vote/how-it-works");
    cy.visit("/#/vote");
    cy.wait(25000);
  }

  getVoteDangerText() {
    return cy.get(".vote-text-danger");
  }

  setAllVotesValue(val) {
    cy.get(".vote-form .vote-form-asset-list-item__weight input").each(($el) => {
      cy.wrap($el).type(val);
    });
  }

  setVotesValues(values) {
    cy.get(".vote-form .vote-form-asset-list-item__weight input").each(($el, $index) => {
      if (values[$index] > 0) {
        cy.wrap($el).type(values[$index]);
      }
    });
  }

  isVotesInDescOrder() {
    let prevValue;

    cy.get(".vote-form-asset-list-item__weight input").each(($el) => {
      if (prevValue !== undefined) {
        if (prevValue > $el.val()) {
          return false;
        }
      }
      prevValue = $el.val();
    });
    return true;
  }
}
