/* eslint-disable ui-testing/no-hard-wait */
/* eslint-disable cypress/no-unnecessary-waiting */
import Page from "../page";

export default class AccountPage extends Page {
  constructor() {
    super();
  }

  getProfileReward() {
    return cy.get(".identity-profile__rewards");
  }

  getHistoryButton() {
    return cy.get(".identity-profile__history-button");
  }

  visit() {
    cy.visit("/#/account");
    cy.wait(15000);
  }
}
