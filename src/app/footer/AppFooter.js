import { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Medium from './icons/medium.svg';
import Telegram from './icons/telegram.svg';
import Twitter from './icons/twitter.svg';
import './AppFooter.scss';

function AppFooter({ className }) {
  return (
    <div className={classnames('app-footer', className)}>
      <Container className="app-footer__container">
        <Row className="align-items-center">
          <Col className="text-start">
            {new Date().getFullYear()} All rights reserved &nbsp;
            <a href="https://finance.vote" rel="noreferrer" target="_blank">
              finance.vote
            </a>
          </Col>
          <Col className="app-footer__links text-end">
            <a href="https://t.me/financedotvote" rel="noreferrer" target="_blank">
              <img alt="Telegram" src={Telegram} />
            </a>
            &nbsp;
            <a href="https://twitter.com/financedotvote" rel="noreferrer" target="_blank">
              <img alt="Twitter" src={Twitter} />
            </a>
            &nbsp;
            <a href="https://medium.com/@financedotvote" rel="noreferrer" target="_blank">
              <img alt="Medium" src={Medium} />
            </a>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

AppFooter.propTypes = {
  className: PropTypes.string
};

export default memo(AppFooter);
