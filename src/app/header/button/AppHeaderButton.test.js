import snapshot from 'tests/snapshot';
import { withRouter } from 'tests/wrappers/router';
import AppHeaderButton from './AppHeaderButton';

describe('AppHeaderButton component', () => {
  it('should match snapshot', () => {
    snapshot(withRouter(<AppHeaderButton to="/" />));
  });
});
