import FinanceVoteLogo from 'assets/logos/financevote.svg';
import MarketsVoteBSCLogo from 'assets/logos/marketsBSC.svg';
import MarketsVoteETHLogo from 'assets/logos/marketsETH.svg';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { NavLink } from 'react-router-dom';
// import BinanceIcon from 'assets/icons/bnb.png'
// import EthereumIcon from 'assets/icons/eth.png'
import AccountForm from 'components/account/form';
import CustomSignInForm from 'components/account/form/CustomSignInForm';
import { useGlobalState } from 'globalState';
import './AppHeader.scss';
import HamburgerIcon from './HamburgerIcon';
import Button from './button';
import useAppHeader from './useAppHeader';

// const iconMap = {
//   1: EthereumIcon,
//   56: BinanceIcon
// }

const logoMap = {
  default: FinanceVoteLogo,
  1: MarketsVoteETHLogo,
  56: MarketsVoteBSCLogo
};

// const coinMap = {
//   1: '',
//   56: {
//     rpcUrl: 'https://apis.ankr.com/a50c4e5ca57f44fa93ca578cfa3618b0/b8bce2324667b39bccd282f57c97a27d/binance/full/main',
//     name: 'Binance Smart Chain',
//     coinName: 'Binance Coin',
//     symbol: 'BNB',
//     explorerUrl: 'https://bscscan.com'
//   }
// }

// const iconSize = 25

function AppHeader({ className, ...props }) {
  const [walletAddress] = useGlobalState('walletAddress');
  const {
    chainId,
    show,
    showCustomSignIn,
    handleClose,
    handleCloseCustomSignIn,
    handleShow,
    handleShowCustomSignIn,
    isMobileMenuOpened,
    setIsMobileMenuOpened
  } = useAppHeader();

  return (
    <div {...props} className={classnames('app-header', className)}>
      <Container className="app-header__container">
        <Row>
          <Col
            className={classnames('app-header__logo', isMobileMenuOpened && 'mobile')}
            md={3}
            xs={8}
          >
            <NavLink to="/">
              <img
                alt="markets.vote"
                src={logoMap[chainId?.data || 'default']}
                style={{ height: '2rem' }}
              />
            </NavLink>
          </Col>
          <Col className="app-header__menu text-md-end justify-content-md-end" md={9} xs={4}>
            <nav className={classnames('app-header__menu__nav', isMobileMenuOpened && 'mobile')}>
              <a
                className="btn btn-link"
                href="https://mint.fvt.app"
                rel="noreferrer noopener"
                target="_blank"
              >
                Mint an identity
              </a>
              <Button to="/vote">Vote</Button>
              <Button to="/account">Account</Button>
              <Button to="/" onClick={() => handleShow()}>
                {walletAddress ? 'Connected' : 'Connect'}
              </Button>
              <Button to="/vote/how-it-works">Help</Button>
              <Button to="/" onClick={() => handleShowCustomSignIn()}>
                {'\u00A0'}
              </Button>
            </nav>
            <HamburgerIcon
              className={classnames(
                'app-header__menu__hamburger',
                isMobileMenuOpened && 'is-active'
              )}
              onClick={() => setIsMobileMenuOpened(!isMobileMenuOpened)}
            />
            {isMobileMenuOpened ? <div className="app-header__menu__backdrop" /> : null}
          </Col>
        </Row>
      </Container>
      <AccountForm show={show} onClose={handleClose} />
      <CustomSignInForm show={showCustomSignIn} onClose={handleCloseCustomSignIn} />
    </div>
  );
}

AppHeader.propTypes = {
  className: PropTypes.string
};

export default memo(AppHeader);
