import { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

export default function useApp() {
  const location = useLocation();

  const navigate = useNavigate();

  useEffect(() => {
    const { pathname } = location;

    switch (pathname) {
      case '/vote':
        if (!localStorage.getItem('tutorialSeen')) {
          navigate('/vote/how-it-works');
        }

        break;
      case '/vote/how-it-works':
        localStorage.setItem('tutorialSeen', true);
        break;
      default:
        break;
    }
  }, [location, navigate]);

  return null;
}
