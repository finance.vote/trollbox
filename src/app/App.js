import Spinner from 'components/indicators/Spinner';
import Store from 'context/identity/Store';
import GlobalStateProvider from 'globalState';
import { lazy, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import ErrorBoundry from 'utils/ErrorBoundry';
import './App.scss';
import Footer from './footer';
import Header from './header';
import useApp from './useApp';

const Home = lazy(() => import('pages/home'));

const Account = lazy(() => import('pages/account'));

const AccountIdentity = lazy(() => import('pages/account/identity'));

const HowItWorks = lazy(() => import('pages/how-it-works'));

const Vote = lazy(() => import('pages/vote'));

const Test = lazy(() => import('pages/test'));

function App() {
  useApp();

  return (
    <div className="app">
      <Header className="app__header" />
      <div className="app__body">
        <ErrorBoundry>
          <Suspense fallback={<Spinner />}>
            <GlobalStateProvider>
              <Store>
                <Routes>
                  <Route element={<Account />} path="/account" />
                  <Route element={<AccountIdentity />} path="/account/identity/:tokenId" />
                  <Route element={<Vote />} path="/vote" />
                  <Route element={<HowItWorks />} path="/vote/how-it-works" />
                  <Route element={<Test />} path="/test" />
                  <Route element={<Home />} index path="/" />
                </Routes>
              </Store>
            </GlobalStateProvider>
          </Suspense>
        </ErrorBoundry>
      </div>
      <Footer className="app__footer" />
    </div>
  );
}

export default App;
