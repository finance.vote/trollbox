import { ethInstance } from 'evm-chain-scripts';
import { useEffect } from 'react';
import { createGlobalState } from 'react-hooks-global-state';
import { getChainSettingsMemoize } from 'utils/helpers';

const initialState = {
  walletAddress: '',
  chainData: getChainSettingsMemoize(process.env.REACT_APP_DEFAULT_CHAIN_ID ?? 137)
};

export const { useGlobalState, setGlobalState, getGlobalState } = createGlobalState(initialState);

const GlobalStateProvider = ({ children }) => {
  const [, setWalletAddress] = useGlobalState('walletAddress');

  const handleAddressChange = async () => {
    const acc = await ethInstance.getEthAccount(false);

    setWalletAddress(acc);
  };

  useEffect(() => {
    document.addEventListener('account_changed', handleAddressChange);

    (async () => {
      const account = await ethInstance.getEthAccount();

      if (account) setWalletAddress(account);
    })();
  }, []);

  return children;
};

export default GlobalStateProvider;
