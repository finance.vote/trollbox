import { Context } from 'context/identity/Store';
import { useGlobalState } from 'globalState';
import moment from 'moment';
import { useCurrentRoundId, useCurrentRoundState } from 'queries/token';
import { useCallback, useContext, useMemo, useState } from 'react';
import {
  getDaysBack,
  getPricesForRound,
  getTokenListRound,
  getTournament,
  identityAlreadyVoted
} from 'utils/simpleGetters';

import { ethInstance } from 'evm-chain-scripts';
import { useEffect } from 'react';
import { getIdentitiesForAccount, getVoiceCredits, getVoteCost } from 'utils/simpleGetters';

const tournamentId = process.env.REACT_APP_TOURNAMENT_ID;

export default function useVotePage() {
  const [isWalletConnected] = useGlobalState('walletAddress');
  const [tournament, setTournament] = useState(null);

  const [dates, setDates] = useState({
    voting: '---',
    market: '---'
  });

  const [tokenList, setTokenList] = useState([]);

  const [payoutDate, setPayoutDate] = useState(null);

  const [startDate, setStartDate] = useState(null);

  const [endDate, setEndDate] = useState(null);

  const [roundBonus, setRoundBonus] = useState(0);

  const [vPerRound, setVPerRound] = useState(0);

  const [voteCost, setVoteCost] = useState(0);

  const [identities, setIdentities] = useState(null);

  const [selectedIdentity, setSelectedIdentity] = useState(null);

  const [getIdentitiesFailed, setGetIdentitiesFailed] = useState(false);

  const [roundProgress, setRoundProgress] = useState(0);

  const [tournamentSteps, setTournamentSteps] = useState([]);

  const [alreadyVoted, setAlreadyVoted] = useState(false);

  const [interfaceDisabled, setInterfaceDisabled] = useState(false);

  const roundId = useCurrentRoundId(tournamentId);

  const roundState = useCurrentRoundState(tournamentId);

  const [state] = useContext(Context);

  const identitySelectableOptions = useMemo(
    () =>
      identities
        ? Object.keys(identities).map((identity) => ({
            value: identity,
            label: `FVTSBT ${identity}`
          }))
        : [],
    [identities]
  );

  const defaultSelectableIdentity = useMemo(() => {
    return identitySelectableOptions?.find(({ value }) => value === selectedIdentity?.tokenId);
  }, [identitySelectableOptions, selectedIdentity]);

  const formatWindowsDate = (start, end) => {
    return `${moment(start).format('HH:mm')} - ${moment(end).format('HH:mm')} UTC ${moment(
      end
    ).format('Do MMM YYYY')}`;
  };

  const updateTimes = useCallback(() => {
    console.log('updateTimes');

    if (tournament && Object.keys(tournament).length > 0) {
      const startTime = tournament.startTime;
      const roundLength = tournament.roundLengthSeconds;
      const roundEnd = startTime + roundId.data * roundLength;
      const payDay = startTime + (roundId.data + 1) * roundLength;
      const payDate = new Date(payDay * 1000);
      const roundStart = startTime + (roundId.data - 1) * roundLength;
      const roundStartDate = new Date(roundStart * 1000);
      const roundEndDate = new Date(roundEnd * 1000);
      const roundProgress = ((new Date() / 1000 - roundStart) / (roundEnd - roundStart)) * 100;
      const voteMax = (tournament.votingPeriodLengthSeconds / roundLength) * 100;
      const marketsMax = (tournament.marketPeriodLengthSeconds / roundLength) * 100;

      const votingCounter = roundStart * 1000 + tournament.votingPeriodLengthSeconds * 1000;
      const marketCounter = votingCounter + tournament.marketPeriodLengthSeconds * 1000;

      const timeZoneOffset = roundStartDate.getTimezoneOffset() * 60;
      const utcRoundStartTime = (roundStart + timeZoneOffset) * 1000;
      const votingStartDate = new Date(utcRoundStartTime);
      const votingTime = utcRoundStartTime + tournament.votingPeriodLengthSeconds * 1000;
      const votingEnd = votingTime - 60000;
      const votingEndDate = new Date(votingEnd); // -1 minute to show e.g 23.59 instead of 0:00 because 0:00 is start of market state
      const marketStartDate = new Date(votingTime);
      const marketEnd = votingTime + tournament.marketPeriodLengthSeconds * 1000;
      const marketEndDate = new Date(marketEnd);

      // console.log('useVotePage', { roundStart, votingStartDate, votingTime, votingEnd });
      // console.log('useVotePage', { votingEndDate, marketStartDate, marketEnd, marketEndDate });

      const windowsDate = {
        voting: formatWindowsDate(votingStartDate, votingEndDate),
        market: formatWindowsDate(marketStartDate, marketEndDate)
      };

      setDates(windowsDate);

      const parts = [
        { min: 0, max: voteMax },
        { min: voteMax, max: voteMax + marketsMax },
        { min: voteMax + marketsMax, max: 100 }
      ];

      const tournamentSteps = [
        {
          ...parts[0],
          name: 'Vote',
          now: roundProgress,
          isCurrent: roundProgress < parts[0].max,
          countDownValue: votingCounter
        },
        {
          ...parts[1],
          name: 'Market',
          now: roundProgress - voteMax > 0 ? roundProgress : 0,
          isCurrent: roundProgress >= parts[1].min && roundProgress < parts[1].max,
          countDownValue: marketCounter
        },
        {
          ...parts[2],
          name: 'Challenge',
          now: roundProgress - marketsMax > 0 ? roundProgress : 0,
          isCurrent: roundProgress >= parts[2].min && roundProgress <= parts[2].max,
          countDownValue: roundEnd * 1000
        }
      ];

      console.log({ roundEnd, payDay, payDate, roundStart, roundLength });

      setPayoutDate(payDate);
      setStartDate(roundStartDate);
      setEndDate(roundEndDate);
      setRoundProgress(roundProgress);
      setTournamentSteps(tournamentSteps);
    }
  }, [roundId.data, tournament]);

  const updateTournament = useCallback(async () => {
    console.log('updateTournament');

    try {
      if (tournamentId > 0) {
        const tournament = await getTournament(tournamentId);
        const { currentRoundId } = tournament;

        let tokenList = await getTokenListRound(tournamentId, currentRoundId);
        const prices = await getPricesForRound(tournamentId, currentRoundId, tokenList);

        const daysBack = getDaysBack(tournament, 0);

        tokenList = tokenList.map((row, index) => ({
          ...row,
          name: row.tickerSymbol,
          index: ++index,
          icon: `assets/icons/${row.tickerSymbol.toLowerCase()}.png`,
          price: {
            symbol: '$',
            value: parseFloat(
              prices[row.tickerSymbol]
                ? prices[row.tickerSymbol].finalPrice
                : prices[row.address].value
            ),
            difference: prices[row.tickerSymbol]
              ? prices[row.tickerSymbol].difference
              : prices[row.address]['7d change'] * 100
          },
          daysBack
        }));

        setTournament(tournament);
        setTokenList(tokenList);
        setRoundBonus(parseInt(tournament.roundRewardTokens));
      }
    } catch (err) {
      console.error(err);
    }
  }, []);

  useEffect(() => {
    async function f() {
      const [vc, cost] = await Promise.all([
        getVoiceCredits(tournamentId),
        getVoteCost(tournamentId)
      ]);
      setVPerRound(vc);
      setVoteCost(parseInt(cost));
    }

    f();
  }, []);

  useEffect(() => {
    if (roundState.data !== 1) {
      setInterfaceDisabled(true);
    } else {
      setInterfaceDisabled(false);
    }
  }, [roundState.data]);

  useEffect(() => {
    if (!selectedIdentity?.tokenId) return;
    (async () => {
      setAlreadyVoted(
        await identityAlreadyVoted(selectedIdentity?.tokenId, process.env.REACT_APP_TOURNAMENT_ID)
      );
    })();
  }, [selectedIdentity]);

  const getIdentities = useCallback(async () => {
    console.log('getIdentities');
    let identityIsSet = false;

    try {
      if (!Object.keys(state.identities).length) {
        const ids = await getIdentitiesForAccount(tournamentId);

        setIdentities(ids);

        if (!identityIsSet && Object.keys(ids).length > 0) {
          setSelectedIdentity(Object.values(ids)[0]);

          identityIsSet = true;
        }
      } else {
        setIdentities(state.identities);
        setSelectedIdentity(Object.values(state.identities)[0]);
      }
    } catch (err) {
      setGetIdentitiesFailed(true); // TODO consider changing this to a better connection checker
      console.error(err);
    }
    console.log(state.identities);
  }, [state.identities, isWalletConnected]);

  function updateRoundId() {
    roundId.refetch();
  }

  function handleIdentityChange({ value }) {
    console.log('handleIdentityChange', value);
    setSelectedIdentity(identities[value]);
  }

  function handleConnectWallet() {
    ethInstance.connectWallet(true);
  }

  useEffect(() => {
    updateTournament();
    getIdentities();
  }, [getIdentities, updateTournament]);

  useEffect(() => {
    updateTimes();
  }, [updateTimes]);

  return {
    defaultSelectableIdentity,
    endDate,
    getIdentitiesFailed,
    handleConnectWallet,
    handleIdentityChange,
    identitySelectableOptions,
    payoutDate,
    roundBonus,
    roundId,
    roundProgress,
    selectedIdentity,
    startDate,
    tokenList,
    tournament,
    tournamentId,
    updateRoundId,
    voteCost,
    vPerRound,
    tournamentSteps,
    isWalletConnected,
    alreadyVoted,
    dates,
    interfaceDisabled
  };
}
