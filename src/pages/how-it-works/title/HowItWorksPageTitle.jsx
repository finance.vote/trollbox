import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import './HowItWorksPageTitle.scss';

function HowItWorksPageTitle({ className, step, title, ...props }) {
  return (
    <div {...props} className={classnames('how-it-works-page-title', className)}>
      {step && <h3>{step}</h3>}
      <h2>{title}</h2>
    </div>
  );
}

HowItWorksPageTitle.propTypes = {
  className: PropTypes.string,
  step: PropTypes.string,
  title: PropTypes.string
};

export default memo(HowItWorksPageTitle);
