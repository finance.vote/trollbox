import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import './HowItWorksPageContainedRow.scss';

function HowItWorksPageContainedRow({ className, children, ...props }) {
  return (
    <Row {...props} className={classnames('how-it-works-page-contained-row', className)}>
      <Col>
        <Container>
          <Row>{children}</Row>
        </Container>
      </Col>
    </Row>
  );
}

HowItWorksPageContainedRow.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};

export default memo(HowItWorksPageContainedRow);
