import classnames from 'classnames';
import Page from 'pages/Page';
import PropTypes from 'prop-types';
import { Fragment, memo } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ButtonGroup from './button-group';
import ConfirmConnectionWithMetaMask from './confirm-connection-with-metamask.png';
import ConfirmVoteTransaction from './confirm-vote-transaction.png';
import ConnectWithMetaMask from './connect-with-metamask.png';
import ContainedRow from './contained-row';
import Countdown from './countdown.svg';
import './HowItWorksPage.scss';
import IdentitySelector from './identity-selector.svg';
import MarketClose from './market-close.svg';
import MarketWindows from './market-windows.svg';
import MetaMask from './metamask.svg';
import PlaceVotes from './place-votes.png';
import Requirement from './requirement';
import Research from './research.svg';
import RewardPool from './reward-pool.svg';
import Title from './title';

function HowItWorksPage({ className }) {
  return (
    <Fragment>
      <Page
        className={classnames('how-it-works-page', className)}
        fluid
        fluidTitle={false}
        fullWidth
        howToPlay={false}
        leftContent={
          <Container fluid>
            <Container>
              <Row>
                <Col className="text-end">
                  <Link className="btn btn-link" to="/vote">
                    Skip tutorial &gt;&gt;
                  </Link>
                </Col>
              </Row>
            </Container>
            <ContainedRow>
              <Col md={7}>
                <p className="how-it-works-page__intro">
                  The finance.vote vote markets are live. Here you can learn the step by step
                  process of casting your quadratic votes on-chain, and making market predictions
                  that have the potential to earn you a share of the $FVT reward pool.
                </p>
                <ButtonGroup />
              </Col>
              <Col md={5}>
                <div className="how-it-works-page__container1">
                  You will need an $FVT Decentralised Identity Token to play, you can mint one using
                  $FVT
                  <a
                    href="https://mint.factorydao.org/fvt"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    {' here'}
                  </a>
                </div>
              </Col>
            </ContainedRow>
            <ContainedRow className="how-it-works-page__requirements">
              <Col>
                <Requirement
                  content={
                    <Fragment>
                      You will need Metamask to connect with the app. Make sure to select the
                      account which is holding your $FVT Decentralised Identity Token.
                    </Fragment>
                  }
                  icon={<img alt="" src={MetaMask} />}
                />
              </Col>
              <Col>
                <Requirement
                  content={
                    <Fragment>
                      Always research the tokens in the token lists to make informed judgements on
                      the future potential of their market value.
                    </Fragment>
                  }
                  icon={<img alt="" src={Research} />}
                />
              </Col>
              <Col>
                <Requirement
                  content={
                    <Fragment>
                      $V Power is the amount of vote power you have to spend.
                      <br />
                      Votes costs the square of the vote number e.g. 5 votes = 5 x 5 = 25
                    </Fragment>
                  }
                  icon={
                    <div className="how-it-works-page__votes-cost-equation how-it-works-page__container1">
                      Votes&sup2; = $V cost
                    </div>
                  }
                />
              </Col>
            </ContainedRow>
            <ContainedRow className="how-it-works-page__step1">
              <Col md={6}>
                <Title step="Step 1" title="Connect your wallet" />
                <p>If your metamask doesn’t automatically connect to our application.</p>
                <p>
                  Hit <span className="highlight">&quot;Connect Wallet&quot;</span>
                </p>
                <p>Metamask will pop up and you will need to approve the application.</p>
                <p>
                  Select which account you would like to connect with the application. Make sure to
                  select the account which is holding your Decentralised Identity Token.
                </p>
                <p>
                  Hit <span className="highlight">&quot;Connect&quot;</span>
                </p>
              </Col>
              <Col className="how-it-works-page__image-group" md={6}>
                <img alt="" src={ConnectWithMetaMask} />
                <img alt="" src={ConfirmConnectionWithMetaMask} />
              </Col>
            </ContainedRow>
            <ContainedRow className="how-it-works-page__step2">
              <Col className="how-it-works-page__voting-screen">
                <Title step="Step 2" title="Get familiar with the voting screen" />
                <Container className="how-it-works-page__container-group" fluid>
                  <Row className="how-it-works-page__container2">
                    <Col sm={12} md={5}>
                      <img alt="" src={IdentitySelector} />
                    </Col>
                    <Col md={7}>
                      Select an Identity from the dropdown. $V Balance is the amount of vote power
                      you have to spend with your selected identity.
                    </Col>
                  </Row>
                  <Row className="how-it-works-page__container2">
                    <Col md={5}>
                      Time left, vote start and vote end date: how long you have left to vote in the
                      voting window.
                    </Col>
                    <Col className="text-end" sm={12} md={7}>
                      <img alt="countdown" src={Countdown} />
                    </Col>
                  </Row>
                  <Row className="how-it-works-page__container2">
                    <Col md={5}>
                      <img alt="reward-pool" src={RewardPool} />
                    </Col>
                    <Col md={7}>
                      Reward Pool: how much $FVT will be shared across the winners of this
                      tournament round.
                    </Col>
                  </Row>
                  <Row className="how-it-works-page__container2">
                    <Col md={6}>
                      Market Close, is the date that you will be able to claim your winnings and
                      when token performance is settled from an on-chain oracle.
                    </Col>
                    <Col className="text-end" md={6}>
                      <img alt="" src={MarketClose} />
                    </Col>
                  </Row>
                </Container>
              </Col>
            </ContainedRow>
            <ContainedRow className="how-it-works-page__step3">
              <Col md={6}>
                <Title step="Step 3" title="Place your votes" />
                <p>Place your votes by changing the values in the $Vote boxes</p>
                <p>
                  The amount of $V spent is the square of your votes i.e: Votes
                  <sup>2</sup>= $V
                </p>
                <p>For example, 5 Votes = 5 x 5 = 5&sup2; = 25 $V</p>
                <p>
                  Rebalance your $V balance if you overspend. A zero $V balance is optimal, but not
                  necessary.
                </p>
                <p>
                  Once you’re happy with your choices.
                  <br />
                  Hit <span className="highlight">&quot;VOTE&quot;</span>
                </p>
              </Col>
              <Col className="text-end" md={6}>
                <img
                  alt=""
                  src={PlaceVotes}
                  // style={{ width: '80%' }}
                />
              </Col>
            </ContainedRow>
            <ContainedRow className="how-it-works-page__step4">
              <Col md={4}>
                <img alt="" src={ConfirmVoteTransaction} style={{ width: '100%' }} />
              </Col>
              <Col md={8}>
                <Title step="Step 4" title="Confirm your transaction" />
                <p>
                  Metamask will open automatically and present you with the option to confirm your
                  transaction.
                </p>
                <p>
                  Gas fees are part of the game. There’s no such thing as a free lunch. Feel free to
                  experiment with gas fees. The more items you pick in the list, the higher the gas
                  fees are.
                </p>
              </Col>
            </ContainedRow>
            <Row className="how-it-works-page__step5">
              <Col className="text-center">
                <Container>
                  <Row>
                    <Col>
                      <Title step="Step 5" title="Your vote has been submitted" />
                      <p className="highlight">
                        Rejoice in the splendour of decentralised quadratic voting as your
                        transaction is mined and committed to the blockchain for eternity!
                      </p>
                      <p>
                        Wait patiently for the markets to do their thing and keep an eye out for
                        market close to see if your choices are correct. If you are, you will be
                        able to claim your $FVT and new voting power when you return.
                      </p>
                      <ButtonGroup />
                    </Col>
                  </Row>
                </Container>
              </Col>
            </Row>
            <Row className="how-it-works-page__step6">
              <Col>
                <Container>
                  <Title title="Understanding the Vote / Market Window" />
                  <Row>
                    <Col md={6}>
                      <h4>Vote window</h4>
                      <p>
                        In the vote window, you have a period of time (usually 1 week) to submit
                        your vote, allocating your preferred market ordering. When this window
                        closes no more votes are submittable.
                      </p>
                    </Col>
                    <Col md={6}>
                      <h4>Market window</h4>
                      <p>
                        Here, your predictions play out in the market. At the closing block of the
                        market window, a snapshot of the top gaining token is determined by a market
                        oracle and is compared against the consensus state.
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="text-center">
                      <img alt="" src={MarketWindows} style={{ width: '100%' }} />
                      <br />
                      <br />
                      <br />
                      <a
                        className="btn btn-primary"
                        href="https://f646e629-e6ab-436e-b4b5-ad62237799a8.filesusr.com/ugd/2f63aa_acbd8518e6cb4b20ae956ef661f85335.pdf"
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        Read the whitepaper
                      </a>
                    </Col>
                  </Row>
                </Container>
              </Col>
            </Row>
          </Container>
        }
        title="How to play the vote markets"
      />
    </Fragment>
  );
}

HowItWorksPage.propTypes = {
  className: PropTypes.string
};

export default memo(HowItWorksPage);
