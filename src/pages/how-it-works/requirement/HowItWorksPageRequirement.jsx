import PropTypes from 'prop-types';
import { Fragment, memo } from 'react';
import './HowItWorksPageRequirement.scss';

function HowItWorksPageRequirement({ content, icon }) {
  return (
    <Fragment>
      <div className="how-it-works-page-requirement__icon">{icon}</div>
      <p className="how-it-works-page-requirement__content">{content}</p>
    </Fragment>
  );
}

HowItWorksPageRequirement.propTypes = {
  className: PropTypes.string,
  content: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  icon: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};

export default memo(HowItWorksPageRequirement);
