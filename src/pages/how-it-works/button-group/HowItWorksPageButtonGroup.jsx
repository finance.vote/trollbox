import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import { Link } from 'react-router-dom';
import './HowItWorksPageButtonGroup.scss';

function HowItWorksPageButtonGroup({ className, ...props }) {
  return (
    <div {...props} className={classnames('how-it-works-page-button-group', className)}>
      <Link className="btn btn-secondary" to="/vote">
        Start voting
      </Link>
      <a
        className="btn btn-outline-secondary"
        href="https://financedotvote.medium.com/finance-vote-how-to-play-the-vote-markets-1ca9a7866e11"
        rel="noopener noreferrer"
        target="_blank"
      >
        Learn more
      </a>
    </div>
  );
}

HowItWorksPageButtonGroup.propTypes = {
  className: PropTypes.string
};

export default memo(HowItWorksPageButtonGroup);
