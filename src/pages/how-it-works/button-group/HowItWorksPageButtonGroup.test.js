import snapshot from 'tests/snapshot';
import { withRouter } from 'tests/wrappers/router';
import HowItWorksPageButtonGroup from './HowItWorksPageButtonGroup';

describe('HowItWorksPageButtonGroup component', () => {
  it('should match snapshot', () => {
    snapshot(withRouter(<HowItWorksPageButtonGroup />));
  });
});
