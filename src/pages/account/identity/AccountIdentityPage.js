import classnames from 'classnames';
import Identity from 'components/identity';
import Page from 'pages/Page';
import PropTypes from 'prop-types';
import { memo } from 'react';
import './AccountIdentityPage.scss';
import useAccountIdentityPage from './useAccountIdentityPage';

function AccountIdentityPage({ className, ...props }) {
  const { tokenId } = useAccountIdentityPage();

  return (
    <Page
      {...props}
      className={classnames('account-identity-page', className)}
      fullWidth
      howToPlay={false}
      leftContent={<Identity tokenId={tokenId} />}
    />
  );
}

AccountIdentityPage.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};

export default memo(AccountIdentityPage);
