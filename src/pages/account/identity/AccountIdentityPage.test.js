import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import AccountIdentityPage from './AccountIdentityPage';

test('should match snapshot', () => {
  const tree = renderer
    .create(
      <MemoryRouter>
        <AccountIdentityPage />
      </MemoryRouter>
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
