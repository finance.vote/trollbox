import classnames from 'classnames';
import Account from 'components/account';
import Page from 'pages/Page';
import PropTypes from 'prop-types';
import { memo } from 'react';
import './AccountPage.scss';

function AccountPage({ className }) {
  return (
    <Page
      className={classnames('account-page', className)}
      fullWidth
      howToPlay={false}
      leftContent={<Account />}
    />
  );
}

AccountPage.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};

export default memo(AccountPage);
