import {
  useChainId,
  useClosedVoteBallots,
  useCurrentRoundId,
  useCurrentRoundState,
  usePreviousBallotIds
} from 'queries/token';
import { useEffect, useState } from 'react';
import { getTournament } from 'utils/simpleGetters';

const numBallots = 8;

export default function useHomePage() {
  const [tournamentId] = useState(process.env.REACT_APP_TOURNAMENT_ID);
  const [marketState, setMarketState] = useState();
  const [timeLeft, setTimeLeft] = useState();
  const { data: currentRoundId, error: currentRoundIdError } = useCurrentRoundId(tournamentId);
  const { data: previousBallotsIds, error: previousBallotsIdsError } = usePreviousBallotIds(
    currentRoundId,
    numBallots
  );

  const { data: chainId } = useChainId();

  const {
    data: previousBallots,
    error: previousBallotsError,
    isLoading: previousBallotsAreLoading
  } = useClosedVoteBallots(tournamentId, previousBallotsIds, chainId);

  const { data: roundState } = useCurrentRoundState(tournamentId);

  useEffect(() => {
    console.log({ previousBallots });
  }, [previousBallots]);

  useEffect(() => {
    if (currentRoundIdError) {
      console.error(currentRoundIdError);
    }
    if (previousBallotsIdsError) {
      console.error(previousBallotsIdsError);
    }
    if (previousBallotsError) {
      console.error(previousBallotsError);
    }
  }, [currentRoundIdError, previousBallotsIdsError, previousBallotsError]);

  useEffect(() => {
    (async () => {
      if (!currentRoundId || !tournamentId || !marketState) return;

      const tournament = await getTournament(tournamentId);

      const startTime = tournament.startTime;
      const roundLength = tournament.roundLengthSeconds;

      const roundStart = startTime + (currentRoundId - 1) * roundLength;

      const votingTime = roundStart * 1000 + tournament.votingPeriodLengthSeconds * 1000;
      const votingEnd = votingTime;

      const marketEnd = votingTime + tournament.marketPeriodLengthSeconds * 1000;
      const nextRoundStart = marketEnd + tournament.challengePeriodLengthSeconds * 1000;

      if (!votingEnd || !nextRoundStart) return;

      if (marketState === 'voting') {
        setTimeLeft(votingEnd);
      } else {
        setTimeLeft(nextRoundStart);
      }
    })();
  }, [tournamentId, currentRoundId, marketState]);

  useEffect(() => {
    let marketState;
    switch (roundState) {
      case 2:
        marketState = 'markets';
        break;
      case 3:
        marketState = 'challenge';
        break;
      case 1:
      default:
        marketState = 'voting';
        break;
    }
    setMarketState(marketState);
  }, [roundState]);

  return {
    marketState,
    timeLeft,
    ballots: previousBallots,
    previousBallotsAreLoading
  };
}
