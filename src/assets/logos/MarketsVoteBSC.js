import Image from './marketsBSC.svg';

function Logo() {
  return <img alt="markets.vote" src={Image} />;
}

export default Logo;
