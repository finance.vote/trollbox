import { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import BSSpinner from 'react-bootstrap/Spinner';
import './Spinner.scss';

function Spinner({ className, overlay, ...props }) {
  return (
    <div className={classnames('spinner', { 'spinner--overlay': overlay }, className)}>
      <BSSpinner animation="border" role="status" {...props}>
        <span className="visually-hidden">Loading...</span>
      </BSSpinner>
    </div>
  );
}

Spinner.propTypes = {
  className: PropTypes.string,
  overlay: PropTypes.bool
};

export default memo(Spinner);
