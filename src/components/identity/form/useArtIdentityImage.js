import { useMemo, useEffect } from 'react';

export default function useArtIdentityImage({ chainId, identityNum } = {}) {
  const url = useMemo(
    () => 'https://gallery.vote/art/chain/' + parseInt(chainId) + '/voter/' + parseInt(identityNum),
    [chainId, identityNum]
  );

  useEffect(() => {
    console.log({ identityNum, chainId });
  }, [chainId, identityNum]);

  return {
    url
  };
}
