import { useEffect } from 'react';
import { useIdentitiesForAccount, useIdentityPrice, useCreateIdentity } from 'queries/token';

export default function useIdentitySelector() {
  const identities = useIdentitiesForAccount();

  const identityPrice = useIdentityPrice();

  const [createIdentity, createdIdentity] = useCreateIdentity();

  useEffect(() => {
    const timeout = setTimeout(identityPrice.refetch, 1000);

    const interval = setInterval(identityPrice.refetch, 15000);

    return () => {
      clearTimeout(timeout);
      clearInterval(interval);
    };
  }, [identities.data, identityPrice.refetch]);

  return {
    createIdentity,
    createdIdentity,
    identities,
    identityPrice
  };
}
