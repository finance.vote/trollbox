import { memo, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card, { CARD_VARIANTS } from 'components/containers/card';
import Ballot from 'components/vote/ballot';
import Spinner from 'components/indicators/Spinner';
import Loader from 'components/containers/loader';
import ArtIdentityImage from '../form/ArtIdentityImage';
import useIdentityCard from './useIdentityCard';
import IdentityMarketCard from './market-card';
import './IdentityCard.scss';

function IdentityCard({ className, tokenId, fvtBalance, voteMarkets, ...props }) {
  const {
    chainId,
    changeBallotNum,
    hasVoteMarkets,
    hasVotesByVoterID,
    selectedBallot,
    selectedBallotIsLoading,
    selectedBallotNum,
    selectedBallotVotes,
    voiceCredits,
    voiceCreditsAreLoading,
    voteMax,
    votesByVoterID,
    votesByVoterIDAreLoading
  } = useIdentityCard({
    tokenId,
    fvtBalance,
    voteMarkets
  });

  return (
    <Row>
      <Col md={6} xs={12}>
        <Card
          {...props}
          className={classnames('identity-card', className)}
          variant={CARD_VARIANTS.DARK}
        >
          {chainId.data && <ArtIdentityImage chainId={chainId.data} identityNum={tokenId} />}
          <Container fluid>
            <Row className="identity-card__header align-items-end">
              <Col className="identity-card__title" md={5} xs={12}>
                Macro
              </Col>
              <Col className="identity-card__balance text-md-end">
                <Loader isLoading={voiceCreditsAreLoading}>
                  $V Power <span title={voiceCredits}>{voiceCredits}</span>
                </Loader>
              </Col>
            </Row>
            {hasVoteMarkets && (
              <Fragment>
                <Row className="identity-card__vote-markets">
                  <Col>
                    <Container className="identity-card__vote-markets__container" fluid>
                      <Row>
                        <Col className="identity-card__vote-markets__wrapper">
                          <Loader isLoading={votesByVoterIDAreLoading}>
                            {hasVotesByVoterID ? (
                              Object.values(votesByVoterID).map((vote, index) => (
                                <IdentityMarketCard
                                  className={classnames({
                                    selected: vote.roundId === selectedBallotNum
                                  })}
                                  key={`identity-market-card${index}`}
                                  name={`#${vote.roundId}`}
                                  rewards={vote.winnings}
                                  roundId={parseInt(vote.roundId)}
                                  showBallot={changeBallotNum}
                                  voterId={tokenId}
                                />
                              ))
                            ) : (
                              <div className="identity-card__vote-markets__message">
                                Identity has not voted
                              </div>
                            )}
                          </Loader>
                        </Col>
                      </Row>
                    </Container>
                  </Col>
                </Row>
              </Fragment>
            )}
          </Container>
        </Card>
      </Col>
      <Col md={6} xs={12}>
        {selectedBallotIsLoading && selectedBallotNum > 0 ? (
          <Spinner />
        ) : (
          selectedBallotNum > 0 &&
          selectedBallot &&
          selectedBallotVotes && (
            <Ballot
              ballotLabel="Your votes"
              ballotNumber={selectedBallot[0].number}
              className="closed-votes__ballot"
              endDate={selectedBallot[0].endDate}
              marketWinner={selectedBallot[0].marketWinner}
              resultsDate={selectedBallot[0].resultsDate}
              showCompletePendingIcon={false}
              sort
              startDate={selectedBallot[0].startDate}
              voteMax={voteMax}
              votes={selectedBallotVotes}
            />
          )
        )}
      </Col>
    </Row>
  );
}

IdentityCard.propTypes = {
  className: PropTypes.string,
  tokenId: PropTypes.string.isRequired,
  fvtBalance: PropTypes.number,
  voteMarkets: PropTypes.array
};

IdentityCard.defaultProps = {
  fvtBalance: 0
};

export default memo(IdentityCard);
