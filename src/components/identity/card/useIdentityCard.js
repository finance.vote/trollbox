import {
  useChainId,
  useClosedVoteBallots,
  useVoiceCredits,
  useVotesByVoterID
} from 'queries/token';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { getTokenListRound } from 'utils/simpleGetters';

export default function useIdentityCard({
  tokenId,
  // fvtBalance,
  voteMarkets
} = {}) {
  const hasVoteMarkets = useMemo(() => voteMarkets?.length > 0, [voteMarkets]);

  const [tournamentId] = useState(process.env.REACT_APP_TOURNAMENT_ID);

  const [votes, setVotes] = useState([]);

  const [selectedBallotNum, setSelectedBallotNum] = useState(0);

  const [voteMax, setVoteMax] = useState(0);

  const [selectedBallotVotes, setSelectedBallotVotes] = useState([]);

  const { data: chainId } = useChainId();

  const {
    data: selectedBallot,
    error: selectedBallotError,
    isLoading: selectedBallotIsLoading
  } = useClosedVoteBallots(tournamentId, [selectedBallotNum], chainId);

  const {
    data: votesByVoterID,
    error: votesByVoterIDError,
    isLoading: votesByVoterIDAreLoading
  } = useVotesByVoterID({
    tokenId,
    tournamentId
  });

  const {
    data: voiceCredits,
    error: voiceCreditsError,
    isLoading: voiceCreditsAreLoading
  } = useVoiceCredits({
    tournamentId
  });

  const hasVotesByVoterID = useMemo(
    () => !!(votesByVoterID && Object.values(votesByVoterID).length),
    [votesByVoterID]
  );

  const changeBallotNum = useCallback(
    (value) => {
      setSelectedBallotNum(value);
      setVotes(votesByVoterID[value]);
    },
    [votesByVoterID]
  );

  const updateSelectedBallotVotes = useCallback(async () => {
    if (selectedBallot && votes.choices) {
      const tokenList = await getTokenListRound(tournamentId, selectedBallot[0].number);

      const stringChoices = votes.choices.map((x) => x.toString());

      const votesResult = tokenList.map((token, i) => {
        const choice = stringChoices.indexOf((i + 1).toString());

        let weight;

        if (choice > -1) {
          weight = parseInt(votes.weights[choice]);
        } else {
          weight = 0;
        }

        return {
          ...token,
          weight,
          icon: `assets/icons/${token.tickerSymbol.toLowerCase()}.png` //logos[token.tickerSymbol.toLowerCase()]
        };
      });

      const max = Math.max(...votesResult.map((x) => x.weight));

      setVoteMax(max);
      setSelectedBallotVotes(votesResult);
    }
  }, [selectedBallot, votes, tournamentId]);

  useEffect(() => {
    if (selectedBallotError) {
      console.error('useIdentityCard error:', selectedBallotError);
    }
  }, [selectedBallotError]);

  useEffect(() => {
    if (voiceCreditsError) {
      console.error('useIdentityCard error:', voiceCreditsError);
    }
  }, [voiceCreditsError]);

  useEffect(() => {
    if (votesByVoterIDError) {
      console.error('useIdentityCard error:', votesByVoterIDError);
    }
  }, [votesByVoterIDError]);

  useEffect(() => {
    updateSelectedBallotVotes();
  }, [updateSelectedBallotVotes]);

  return {
    chainId,
    changeBallotNum,
    hasVoteMarkets,
    hasVotesByVoterID,
    selectedBallot,
    selectedBallotIsLoading,
    selectedBallotNum,
    selectedBallotVotes,
    voiceCredits,
    voiceCreditsAreLoading,
    voteMax,
    votesByVoterID,
    votesByVoterIDAreLoading
  };
}
