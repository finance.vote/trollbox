import snapshot from 'tests/snapshot';
import HowToPlayItem from './HowToPlayItem';

describe('HowToPlayItem component', () => {
  it('should match snapshot', () => {
    snapshot(<HowToPlayItem icon="/mock.png" label="My Label" />);
  });
});
