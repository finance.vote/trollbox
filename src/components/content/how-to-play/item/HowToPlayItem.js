import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import './HowToPlayItem.scss';

function HowToPlayItem({ className, icon, label, ...props }) {
  return (
    <div {...props} className={classnames('how-to-play__item', className)}>
      <img alt={label} className="how-to-play__item__icon" src={icon} />
      <div className="how-to-play__item__label">{label}</div>
    </div>
  );
}

HowToPlayItem.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default memo(HowToPlayItem);
