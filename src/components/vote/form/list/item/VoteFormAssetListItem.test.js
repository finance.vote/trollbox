import snapshot from 'tests/snapshot';
import VoteFormAssetListItem from './VoteFormAssetListItem';

describe('VoteFormAssetListItem component', () => {
  it('should match snapshot', () => {
    snapshot(
      <VoteFormAssetListItem icon="mock.png" name="My Name" symbol="mock" tickerSymbol="mock" />
    );
  });
});
