import classnames from 'classnames';
import Item from 'components/asset-list/item';
import NumberControl from 'components/form/controls/number';
import PropTypes from 'prop-types';
import { memo } from 'react';
import { Col } from 'react-bootstrap';
import useVoteFormAssetListItem from './useVoteFormAssetListItem';
import './VoteFormAssetListItem.scss';

function VoteFormAssetListItem({
  className,
  editable,
  icon,
  index,
  maxVotes,
  name,
  tickerSymbol,
  tokensAvailable,
  weight,
  onChange,
  ...props
}) {
  const { cleanProps, maxProgressValue } = useVoteFormAssetListItem({
    ...props,
    index,
    maxVotes,
    tokensAvailable
  });

  return (
    <Item
      {...cleanProps}
      className={classnames('vote-form-asset-list-item', className)}
      icon={icon}
      name={name}
      progress={(weight * 100) / maxProgressValue}
      tickerSymbol={tickerSymbol}
    >
      <Col xs="auto">
        <fieldset
          className={classnames('vote-form-asset-list-item__weight', editable ? 'editable' : '')}
        >
          <legend>Vote</legend>
          <NumberControl
            min={0}
            placeholder="0"
            readOnly={!editable}
            value={weight}
            onChange={onChange}
            index={index}
          />
        </fieldset>
      </Col>
    </Item>
  );
}

VoteFormAssetListItem.propTypes = {
  className: PropTypes.string,
  editable: PropTypes.bool,
  icon: PropTypes.string,
  index: PropTypes.number,
  maxVotes: PropTypes.number,
  name: PropTypes.string.isRequired,
  showWeight: PropTypes.bool,
  tickerSymbol: PropTypes.string.isRequired,
  tokensAvailable: PropTypes.number,
  weight: PropTypes.number,
  onChange: PropTypes.func
};

export default memo(VoteFormAssetListItem);
