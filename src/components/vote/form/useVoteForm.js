import { useSubmitVote } from 'queries/token';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { TRANSACTION_STATUS } from 'utils/helpers';

export default function useVoteForm({
  items,
  selectedIdentity,
  tokensAvailable,
  tournamentId,
  alreadyVoted,
  interfaceDisabled
} = {}) {
  const [votes, setVotes] = useState({});

  const [cancelPrompt, setCancelPrompt] = useState(false);

  const [currentMaxValue, setCurrentMaxValue] = useState(
    Math.floor(Math.sqrt(tokensAvailable)) ?? 0
  );
  const [tokensSpent, setTokensSpent] = useState(0);
  const [tokensLeft, setTokensLeft] = useState(tokensAvailable ?? 0);

  const {
    data: submitVoteResult,
    error: submitVoteError,
    isIdle: submitVoteIsIdle,
    isLoading: submitVoteIsLoading,
    mutate: submitVote,
    reset: resetSubmitVote
  } = useSubmitVote();

  const hasVoted = useMemo(() => !!submitVoteResult?.hash, [submitVoteResult]);

  const [transactionStatus, setTransactionStatus] = useState(TRANSACTION_STATUS.NONE);

  useEffect(() => {
    async function f() {
      if (submitVoteResult) {
        let transaction;
        try {
          setTransactionStatus(TRANSACTION_STATUS.WAITING);
          transaction = await submitVoteResult.wait();
          console.log('submit vote result after wait', { transaction });
        } catch (err) {
          console.error(err);
        } finally {
          setTransactionStatus(
            transaction && transaction.status === TRANSACTION_STATUS.CONFIRMED
              ? TRANSACTION_STATUS.CONFIRMED
              : TRANSACTION_STATUS.FAILED
          );
        }
      }
    }

    f();
  }, [submitVoteResult]);

  const calcTokensSpent = (_votes) => {
    return Object.values(_votes).reduce(
      (accumulator, currentValue) => accumulator + currentValue ** 2,
      0
    );
  };

  const changeVoteWeight = (index, weight) => {
    const votesToCalc = { ...votes };
    delete votesToCalc[index];

    const _tokensLeft = tokensAvailable - calcTokensSpent(votesToCalc);
    const _currentMaxValue = Math.floor(Math.sqrt(_tokensLeft)) || 0;

    setCurrentMaxValue(_currentMaxValue);

    const parsedValue = parseInt(weight);
    const value = isNaN(parsedValue) ? 0 : parsedValue;
    const resValue = value ** 2 > _tokensLeft ? _currentMaxValue : value;
    const newVotes = { ...votesToCalc, [index]: resValue };

    setVotes(newVotes);
  };

  useEffect(() => {
    const tokensSpent = calcTokensSpent(votes);
    const tokensLeft = tokensAvailable - tokensSpent;
    setTokensSpent(tokensSpent);
    setTokensLeft(tokensLeft);
  }, [votes]);

  const syntheticItems = useMemo(() => {
    const resultItems = items?.map((item, index) => ({
      ...item,
      weight: votes[index] || 0,
      tokensAvailable,
      editable: !!selectedIdentity && !hasVoted && !alreadyVoted && !interfaceDisabled,
      index,
      onChange: changeVoteWeight
    }));

    if (alreadyVoted && resultItems.length) {
      for (let i = 0; i < alreadyVoted[1].length; i++) {
        const index = alreadyVoted[1][i].toNumber() - 1;
        resultItems[index].weight = alreadyVoted[2][i].toNumber();
      }
    }

    if (hasVoted || alreadyVoted) {
      resultItems.sort((a, b) =>
        (a.weight ?? 0) > (b.weight ?? 0) ? -1 : (b.weight ?? 0) > (a.weight ?? 0) ? 1 : 0
      );
    }

    return resultItems;
  }, [hasVoted, items, selectedIdentity, tokensAvailable, votes, alreadyVoted, interfaceDisabled]);

  const resetVotes = useCallback(() => {
    setVotes((votes) => ({
      ...Object.keys(votes).reduce((votes, _, index, keys) => ({ ...votes, [keys[index]]: 0 }), {})
    }));
    resetSubmitVote();
  }, [resetSubmitVote]);

  const handleSubmit = useCallback(async () => {
    const choices = [];

    const userVotes = [];

    Object.entries(votes).forEach(([choice, vote]) => {
      if (parseInt(vote) > 0) {
        choices.push(parseInt(choice) + 1);
        userVotes.push(vote);
      }
    });

    return submitVote({
      choices,
      weights: userVotes,
      voterId: selectedIdentity.tokenId,
      tournamentId
    });
  }, [selectedIdentity, submitVote, tournamentId, votes]);

  const handleCancel = useCallback(() => {
    setCancelPrompt(true);
  }, []);

  const handleHideCancelPrompt = useCallback(() => {
    setCancelPrompt(false);
  }, []);

  const handleExit = useCallback(() => {
    setCancelPrompt(false);
    resetVotes();
  }, [resetVotes]);

  useEffect(() => {
    resetVotes();
  }, [selectedIdentity, resetVotes]);

  useEffect(() => {
    setTokensLeft(tokensAvailable);
  }, [tokensAvailable]);

  return {
    cancelPrompt,
    hasVoted,
    submitVote,
    submitVoteError,
    submitVoteIsIdle,
    submitVoteIsLoading,
    submitVoteResult,
    syntheticItems,
    currentMaxValue,
    tokensSpent,
    tokensLeft,
    handleCancel,
    handleExit,
    handleHideCancelPrompt,
    handleSubmit,
    transactionStatus
  };
}
