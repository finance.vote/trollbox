import { useCallback, useMemo, useState } from 'react';

export default function useClosedVotes({ ballots } = {}) {
  const resolveBallots = useMemo(
    () =>
      ballots?.sort((ballot1, ballot2) => {
        const value1 = ballot1.number;

        const value2 = ballot2.number;

        if (value1 < value2) {
          return 1;
        }

        if (value1 > value2) {
          return -1;
        }

        return 0;
      }),
    [ballots]
  );

  const hasBallots = useMemo(() => resolveBallots?.length > 0, [resolveBallots]);

  const [show, setShow] = useState(false);

  const [ballotNumber, setBallotNumber] = useState(null);

  const [startDate, setStartDate] = useState(null);

  const [resultsDate, setResultsDate] = useState(null);

  const [marketWinner, setMarketWinner] = useState(null);

  const [votes, setVotes] = useState([]);

  const handleClose = useCallback(() => setShow(false), []);

  const handleShow = useCallback(() => setShow(true), []);

  const challengeWinner = useCallback(
    (ballotNumber, startDate, resultsDate, marketWinner, votes) => {
      setBallotNumber(ballotNumber);
      setStartDate(startDate);
      setResultsDate(resultsDate);
      setMarketWinner(marketWinner);
      setVotes(votes);
      handleShow();
    },
    [handleShow]
  );

  return {
    ballotNumber,
    challengeWinner,
    hasBallots,
    marketWinner,
    resolveBallots,
    resultsDate,
    show,
    startDate,
    votes,
    handleClose
  };
}
