import snapshot from 'tests/snapshot';
import VoteBallot from './VoteBallot';

describe('VoteBallot component', () => {
  it('should match snapshot', () => {
    const handleChallengeWinner = jest.fn();

    snapshot(
      <VoteBallot
        ballotLabel="My Ballot"
        ballotNumber={1}
        challengeWinner={handleChallengeWinner}
        endDate={new Date(1).getTime()}
        resultsDate={new Date(2).getTime()}
        startDate={new Date(0).getTime()}
        votes={[]}
      />
    );
  });
});
