import classnames from 'classnames';
import AssetCard, { AssetCardTypes } from 'components/asset-card';
import AssetList from 'components/vote/form/list';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import './VoteBallot.scss';
import CompleteIcon from './complete.svg';
import PendingIcon from './pending.svg';
import useVoteBallot from './useVoteBallot';

function VoteBallot({
  ballotLabel,
  ballotNumber,
  challengeWinner,
  className,
  marketWinner,
  projectedWinner,
  proposal,
  resultsDate,
  showCompletePendingIcon,
  sort,
  startDate,
  voteMax,
  votes
}) {
  const { formattedBallotNumber, fomrattedStartDate, formattedResultsDate, syntheticVotes } =
    useVoteBallot({
      ballotNumber,
      marketWinner,
      resultsDate,
      startDate,
      voteMax,
      votes
    });

  return (
    <Container className={classnames('vote-ballot', className)} fluid>
      <Row>
        <Col className="vote-ballot__label">
          {fomrattedStartDate} - {formattedResultsDate}
        </Col>
      </Row>
      <Row>
        <Col>
          <h1>Ballot #{formattedBallotNumber}</h1>
        </Col>
        {showCompletePendingIcon && (
          <Col className="text-end">
            {!marketWinner && proposal ? (
              <Button
                variant="danger"
                onClick={() =>
                  challengeWinner?.(
                    ballotNumber,
                    startDate,
                    resultsDate,
                    votes[proposal.winnerIndex],
                    votes
                  )
                }
              >
                Challenge Winner
              </Button>
            ) : (
              <img
                alt={marketWinner ? 'complete' : 'pending'}
                className="vote-ballot__icon"
                src={marketWinner ? CompleteIcon : PendingIcon}
              />
            )}
          </Col>
        )}
      </Row>
      <Row className="vote-ballot__result-cards">
        {projectedWinner && (
          <Col>
            <AssetCard
              asset={
                projectedWinner && {
                  icon: projectedWinner.icon,
                  name: projectedWinner.name
                }
              }
              title="Projected winner"
              type={AssetCardTypes.PROJECTED}
            />
          </Col>
        )}
        <Col>
          {!marketWinner && proposal ? (
            <AssetCard
              asset={
                votes[proposal.winnerIndex] && {
                  icon: votes[proposal.winnerIndex].icon,
                  name: votes[proposal.winnerIndex].name
                }
              }
              title="Proposed winner"
              type={AssetCardTypes.PROPOSED}
            />
          ) : (
            <AssetCard
              asset={
                marketWinner && {
                  icon: marketWinner.icon,
                  name: marketWinner.name
                }
              }
              date={resultsDate}
              title="Market winner"
              type={marketWinner ? AssetCardTypes.COMPLETED : AssetCardTypes.PENDING}
            />
          )}
        </Col>
      </Row>
      <Row>
        <Col className="vote-ballot__label">{ballotLabel}</Col>
      </Row>
      <Row>
        <Col>
          <AssetList className="vote-ballot__asset-list" items={syntheticVotes} sort={sort} />
        </Col>
      </Row>
    </Container>
  );
}

VoteBallot.propTypes = {
  className: PropTypes.string,
  ballotLabel: PropTypes.string.isRequired,
  ballotNumber: PropTypes.number.isRequired,
  startDate: PropTypes.number.isRequired,
  endDate: PropTypes.number.isRequired,
  resultsDate: PropTypes.number.isRequired,
  projectedWinner: PropTypes.shape({
    icon: PropTypes.isRequired,
    name: PropTypes.isRequired
  }),
  proposal: PropTypes.shape({
    winnerIndex: PropTypes.number
  }),
  marketWinner: PropTypes.shape({
    icon: PropTypes.isRequired,
    name: PropTypes.isRequired
  }),
  showCompletePendingIcon: PropTypes.bool,
  sort: PropTypes.bool,
  voteMax: PropTypes.number,
  votes: PropTypes.array.isRequired,
  challengeWinner: PropTypes.func
};

VoteBallot.defaultProps = {
  ballotLabel: 'Community votes',
  showCompletePendingIcon: true,
  sort: true
};

export default memo(VoteBallot);
