import { memo, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Alert from 'react-bootstrap/Alert';
import useNetworkChecker from './useNetworkChecker';
import './NetworkChecker.scss';

function NetworkChecker({ className, supportedChains = [] }) {
  const { currentNetwork, show, setShow } = useNetworkChecker({
    supportedChains
  });

  if (show) {
    return (
      <Alert
        className={classnames('network-checker', className)}
        dismissible
        variant="warning"
        onClose={() => setShow(false)}
      >
        Oops, looks like we cannot connect to the blockchain. Your current network is{' '}
        {currentNetwork || 'unknown'}. Supported networks include [{supportedChains.toString()}]
        <br />
        Check which network your ethereum wallet is connected to.
      </Alert>
    );
  }

  return <Fragment />;
}

NetworkChecker.propTypes = {
  className: PropTypes.string,
  supportedChains: PropTypes.arrayOf(PropTypes.number)
};

export default memo(NetworkChecker);
