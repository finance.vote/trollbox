import { useIdentitiesForAccount } from 'queries/token';
import { useCallback, useMemo, useState } from 'react';

export default function useAccount() {
  const tournamentId = process.env.REACT_APP_TOURNAMENT_ID; // TODO: make dynamic

  const [showTransferDialog, setShowTransferDialog] = useState(false);

  const {
    data: identitiesForAccount,
    error: identitiesForAccountError,
    isLoading: identitiesForAccountAreLoading
  } = useIdentitiesForAccount(tournamentId);

  const hasIdentitiesForAccount = useMemo(
    () => !!identitiesForAccount && Object.keys(identitiesForAccount).length > 0,
    [identitiesForAccount]
  );

  const handleCloseTransferDialog = useCallback(() => setShowTransferDialog(false), []);

  const handleShowTransferDialog = useCallback(() => setShowTransferDialog(true), []);

  return {
    hasIdentitiesForAccount,
    identitiesForAccount,
    identitiesForAccountAreLoading,
    identitiesForAccountError,
    showTransferDialog,
    handleCloseTransferDialog,
    handleShowTransferDialog
  };
}
