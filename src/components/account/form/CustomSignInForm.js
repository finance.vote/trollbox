import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Modal from 'react-bootstrap/Modal';
import './AccountForm.scss';

function CustomSignInForm({ className, show, onClose, ...props }) {
  return (
    <Modal
      centered
      {...props}
      className={classnames('account-form', className)}
      show={show}
      onHide={onClose}
    ></Modal>
  );
}

CustomSignInForm.propTypes = {
  className: PropTypes.string,
  show: PropTypes.bool,
  onClose: PropTypes.func
};

export default memo(CustomSignInForm);
