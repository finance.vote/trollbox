import { ethInstance } from 'evm-chain-scripts';
import { useMemo } from 'react';

export default function useAccountForm() {
  const isWalletConnected = useMemo(() => ethInstance.isWalletConnected(), []);

  return {
    isWalletConnected
  };
}
