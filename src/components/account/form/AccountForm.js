import classnames from 'classnames';
import { ConnectWallet } from 'evm-chain-scripts';
import { useGlobalState } from 'globalState';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import './AccountForm.scss';

function AccountForm({ className, show, onClose, ...props }) {
  const [walletAddress] = useGlobalState('walletAddress');
  return (
    <Modal
      {...props}
      centered
      className={classnames('account-form', className)}
      show={show}
      onHide={onClose}
      contentClassName={`modal-content${walletAddress ? '__turq' : '__red'}`}
    >
      <Modal.Body className="text-start">
        {walletAddress ? (
          <h2 className="text-center">Connected wallet:</h2>
        ) : (
          <Row>
            <Col>
              <h2 className="text-center">Connect your wallet</h2>
              Please ensure that metamask or another browser wallet is installed and activated to
              use this feature
            </Col>
          </Row>
        )}
        <Row>
          <Col className="text-center">
            <ConnectWallet className="btn btn-secondary" />
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer className="d-flex justify-content-center">
        <Button
          variant="danger"
          className={walletAddress && 'account-form__close-btn'}
          onClick={onClose}
        >
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

AccountForm.propTypes = {
  className: PropTypes.string,
  show: PropTypes.bool,
  onClose: PropTypes.func
};

export default memo(AccountForm);
