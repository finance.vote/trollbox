import classnames from 'classnames';
import numeral from 'numeral';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Row from 'react-bootstrap/Row';
import './AssetListItem.scss';

function AssetListItem({
  children,
  className,
  icon,
  name,
  price,
  progress,
  tickerSymbol,
  ...props
}) {
  return (
    <Container {...props} className={classnames('asset-list-item', className)}>
      {typeof progress !== 'undefined' && (
        <Row className="asset-list-item__volume">
          <Col>
            <ProgressBar now={progress} />
          </Col>
        </Row>
      )}
      <Row className="gridrow">
        <Col className="align-self-center" xs="auto">
          <img alt="" className="asset-list-item__icon" src={icon} />
        </Col>
        <Col className="align-self-center">
          <Row>
            <Col className="text-start">
              <div className="asset-list-item__name">{name}</div>
              <div className="asset-list-item__symbol">{tickerSymbol}</div>
            </Col>
            {price && (
              <Col className="text-end" xs="auto">
                <div className="asset-list-item__price">
                  {numeral(price.value).format(`${price.symbol} 0,0.00`)}
                </div>
                <div
                  className={classnames(
                    'asset-list-item__price-difference',
                    `asset-list-item__price-difference--${
                      price.difference > 0 ? 'positive' : 'negative'
                    }`
                  )}
                >
                  {numeral(price.difference).format('+ 0,0.00')}% &nbsp;
                  {/* <span className="text-muted">{daysBack}d</span> */}
                </div>
              </Col>
            )}
          </Row>
        </Col>
        {children}
      </Row>
    </Container>
  );
}

AssetListItem.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  // daysBack: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  icon: PropTypes.string,
  name: PropTypes.string.isRequired,
  price: PropTypes.shape({
    symbol: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    difference: PropTypes.number.isRequired
  }),
  progress: PropTypes.number,
  tickerSymbol: PropTypes.string.isRequired
};

// AssetListItem.defaultProps = {
//   daysback: 7
// };

export default memo(AssetListItem);
