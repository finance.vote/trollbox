import classnames from 'classnames';
import PropTypes from 'prop-types';
import { forwardRef, Fragment, memo } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { AutoSizer } from 'react-virtualized';
import './AssetList.scss';
import Item from './item';
import useAssetList from './useAssetList';

const AssetList = forwardRef(({ className, items, itemComponent: ItemComponent, sort }, ref) => {
  const { hasItems } = useAssetList({ items });

  return (
    <div
      ref={ref}
      className={classnames('asset-list', { 'asset-list--empty': !hasItems }, className)}
    >
      {hasItems ? (
        <AutoSizer>
          {({ width, height }) => (
            <Scrollbars
              renderThumbHorizontal={(props) => <div {...props} style={{ display: 'none' }} />}
              renderThumbVertical={(props) => (
                <div {...props} className="asset-list__scrollbars__thumb" />
              )}
              renderView={(props) => <div {...props} className="asset-list__scrollbars__view" />}
              style={{ width, height }}
            >
              {items.sort(sort || (() => 0)).map((item) => (
                <ItemComponent
                  key={`item${item.tickerSymbol}`}
                  {...item}
                  className={classnames('asset-list__item', item.className)}
                />
              ))}
            </Scrollbars>
          )}
        </AutoSizer>
      ) : (
        <Fragment>List is empty</Fragment>
      )}
    </div>
  );
});
AssetList.displayName = 'AssetList';
AssetList.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  itemComponent: PropTypes.oneOfType([PropTypes.element, PropTypes.object]),
  sort: PropTypes.func
};

AssetList.defaultProps = {
  itemComponent: Item
};

export default memo(AssetList, (prev, next) => {
  return JSON.stringify(prev) === JSON.stringify(next);
});
