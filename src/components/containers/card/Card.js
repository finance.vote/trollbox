import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import './Card.scss';

export const CARD_VARIANTS = {
  LIGHT: 'light',
  DARK: 'dark'
};

function Card({ className, variant, children, ...props }) {
  return (
    <div
      {...props}
      className={classnames('card', { 'card--dark': variant === CARD_VARIANTS.DARK }, className)}
    >
      {children}
    </div>
  );
}

Card.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  variant: PropTypes.string
};

Card.defaultProps = {
  variant: CARD_VARIANTS.LIGHT
};

export default memo(Card);
