import PropTypes from 'prop-types';
import { memo } from 'react';
import Form from 'react-bootstrap/Form';
import useNumberControl from './useNumberControl';

function NumberControl({ index, min, readOnly, value, onChange, ...props }) {
  const { controlRef, handleChange, handleFocus, handleKeyDown } = useNumberControl({
    readOnly,
    value,
    onChange,
    index
  });

  return (
    <Form.Control
      {...props}
      min={min}
      placeholder="0"
      readOnly={readOnly}
      ref={controlRef}
      type="text"
      value={value}
      onChange={handleChange}
      onFocus={handleFocus}
      onKeyDown={handleKeyDown}
    />
  );
}

NumberControl.propTypes = {
  className: PropTypes.string,
  min: PropTypes.number,
  readOnly: PropTypes.bool,
  value: PropTypes.number,
  onChange: PropTypes.func
};

export default memo(NumberControl);
