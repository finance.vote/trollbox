import { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';
import './NavLinkButton.scss';

function NavLinkButton({ className, to, children, ...props }) {
  return (
    <div {...props} className={classnames('nav-link-button', className)}>
      <NavLink activeClassName="active" to={to}>
        {children}
      </NavLink>
    </div>
  );
}

NavLinkButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string
  ]),
  to: PropTypes.string.isRequired
};

export default memo(NavLinkButton);
