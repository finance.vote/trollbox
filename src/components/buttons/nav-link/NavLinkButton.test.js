import snapshot from 'tests/snapshot';
import { withRouter } from 'tests/wrappers/router';
import NavLinkButton from './NavLinkButton';

describe('NavLinkButton component', () => {
  it('should match snapshot', () => {
    snapshot(withRouter(<NavLinkButton to="/" />));
  });
});
