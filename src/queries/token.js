import { ethInstance } from 'evm-chain-scripts';
import { useMutation, useQuery } from 'react-query';
import {
  ROUND_STATE,
  getBallotForRound,
  getClosedBallotForRound,
  getCurrentRoundId,
  getCurrentRoundState,
  getIdentitiesForAccount,
  getNumIdentities,
  getRoundState,
  getTokenBalance,
  getTournament,
  getVoiceCredits,
  getVotesByVoterID
} from 'utils/simpleGetters';
import { submitVote } from 'utils/simpleSetters';

export const useNumIdentities = () => useQuery('numIdentities', () => getNumIdentities());

export const useIdentitiesForAccount = (tournamentId) =>
  useQuery('getIdentitiesForAccount', () => getIdentitiesForAccount(tournamentId));

export const useCurrentRoundId = (tournamentId) =>
  useQuery('currentRoundId', () => getCurrentRoundId(tournamentId));

export const useCurrentRoundState = (tournamentId) =>
  useQuery('roundState', () => getCurrentRoundState(tournamentId));

export const useTokenBalance = () => useQuery('tokenBalance', getTokenBalance);

export const useSubmitVote = (options) =>
  useMutation((variables) => submitVote(variables), options);

export const useChainId = () =>
  useQuery('chainId', () => {
    return Number(ethInstance.getChainId());
  });

export const useAccount = () => useQuery('currentAccount', ethInstance.getEthAccount);

export const useClosedVoteBallots = (tournamentId, roundIds, chainId) =>
  useQuery(
    ['closedVoteBallots', tournamentId, roundIds, chainId],
    async () => {
      if (roundIds?.length > 0) {
        const tournament = await getTournament(tournamentId);

        const ballots = [];

        for (let i = 0; i < roundIds.length; i++) {
          const roundId = roundIds[i];
          if (roundId === 0) continue;

          ballots.push(getClosedBallotForRound(tournament, roundId, chainId));
        }

        const ballotsResults = (await Promise.all(ballots)).filter((x) => x);

        const currentRoundState = await getRoundState(tournamentId, tournament.currentRoundId);

        if (currentRoundState > ROUND_STATE.VOTING) {
          const currentRoundBallot = await getBallotForRound(tournament, tournament.currentRoundId);
          ballotsResults.push(currentRoundBallot);
        }

        return ballotsResults;
      }

      return [];
    },
    { enabled: !!(roundIds.length && tournamentId && chainId) }
  );

export const useVotesByVoterID = ({ tokenId, tournamentId, ...options }) =>
  useQuery(
    ['votesByVoterID', { tokenId, tournamentId }],
    () => getVotesByVoterID(tokenId, tournamentId),
    options
  );

export const useVoiceCredits = ({ tournamentId, ...options }) =>
  useQuery(['voiceCredits', { tournamentId }], () => getVoiceCredits(tournamentId), options);

export const usePreviousBallotIds = (currentRoundId, numBallots) =>
  useQuery(
    ['previousBallotIds', currentRoundId, numBallots],
    () => {
      let previousBallotIds = [];
      for (let i = 1; i <= numBallots; i++) {
        const prevBallotId = currentRoundId - i;
        if (prevBallotId > 0) previousBallotIds.push(prevBallotId);
      }
      return previousBallotIds;
    },
    { enabled: !!currentRoundId, initialData: [] }
  );
