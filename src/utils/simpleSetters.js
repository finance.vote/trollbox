import PermissionlessMarkets from 'contracts/PermissionlessMarkets.json';
import VoterID from 'contracts/VoterID.json';
import { ethInstance } from 'evm-chain-scripts';
import { addIPFS, getEthBytesFromIpfsHash } from 'utils/ipfs';
import { getApprovalForVoteToken } from './simpleGetters';

const emptyHash = '0x0000000000000000000000000000000000000000000000000000000000000000';

export async function withdrawWinnings(tournamentId, voterId, setError) {
  const contract = await ethInstance.getContract('write', PermissionlessMarkets);

  const account = await ethInstance.getEthAccount();

  try {
    await contract.withdrawWinnings(tournamentId, voterId, {
      from: account,
      gasLimit: 430000
    });
  } catch (err) {
    setError(err);
  }
}

export async function submitVote({ voterId, tournamentId, choices, weights }) {
  const contract = await ethInstance.getContract('write', PermissionlessMarkets);

  const account = await ethInstance.getEthAccount();

  console.log('vote', voterId, tournamentId, choices, weights);

  // 1 vote costs < 150k, 10 votes costs 550k
  // 400k / 9 ~= 45k => gasPrice = 100k + 45k * choices.length

  // const gasLimit = 300000 + 150000 * choices.length;

  const voteCostTokens = await getApprovalForVoteToken(tournamentId, account);

  console.log('submitVote', { voterId, tournamentId, choices, weights });

  const res = await contract.vote(
    voterId,
    tournamentId,
    choices,
    weights,
    voteCostTokens,
    emptyHash,
    {
      from: account
    }
  );

  return res;
}

export async function challengeWinner(claimedWinner) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const account = await ethInstance.getEthAccount();

  const gasLimit = 100000;

  const ipfsHash = await addIPFS({
    version: 1,
    challenger: account,
    evidence: 'evidence stuff'
  });

  const evidence = getEthBytesFromIpfsHash(ipfsHash);

  await contract.challengeWinner(claimedWinner, evidence, {
    from: account,
    gas: gasLimit
  });
}

export async function transferVoterID(identityId, address) {
  try {
    const identity = await ethInstance.getContract('write', VoterID);

    const account = await ethInstance.getEthAccount();

    const gasLimit = 100000;

    await identity.transferFrom(account, address, identityId, {
      from: account,
      gas: gasLimit
    });
  } catch (err) {
    console.error(err);
  }
}
