import MulticallAbi from 'contracts/Multicall.json';
import { ethers } from 'ethers';
import { getChainSettings } from './helpers';

export const MulticallRequest = async (chainId, dataToCalls) => {
  const settings = getChainSettings(chainId);

  if (!settings.multicall || !settings.rpc) return [];

  const { multicall, rpc } = settings;

  const propRpc = rpc[0].startsWith('wss') ? rpc[0].replace('wss', 'https') : rpc[0];

  const provider = new ethers.JsonRpcProvider(propRpc, chainId, {
    batchMaxCount: 100
  });

  const multicallInterface = new ethers.Interface(MulticallAbi);

  const contract = await new ethers.Contract(multicall, multicallInterface, provider);

  const calls = [];

  for (let index = 0; index < dataToCalls.length; index++) {
    const { contractAddress, functionName, args, contractInterface } = dataToCalls[index];

    const newCall = {
      allowFailure: false,
      target: contractAddress,
      callData: contractInterface.encodeFunctionData(functionName, args)
    };

    calls.push(newCall);
  }

  const results = await contract.aggregate3.staticCall(calls);
  const decodedData = [];

  for (let index = 0; index < results.length; index++) {
    const { returnData } = results[index];

    const { contractInterface, functionName } = dataToCalls[index];

    const decoded = contractInterface.decodeFunctionResult(functionName, returnData);

    decodedData.push(decoded);
  }

  return decodedData;
};
