import ChainLinkAggregator from 'contracts/DummyChainLinkAggregator.json';
import PermissionlessMarkets from 'contracts/PermissionlessMarkets.json';
import Token from 'contracts/Token.json';
import VoterID from 'contracts/VoterID.json';
import { ethers } from 'ethers';
import { ethInstance, fromWei, toWei } from 'evm-chain-scripts';
import { MulticallRequest } from './multicall';

export const ROUND_STATE = {
  VOTING: 1,
  MARKET: 2,
  CHALLENGE: 3
};

const tournaments = {};

const rounds = {};

const tokenLists = {};

const decimals = {};

const symbols = {};

export async function getDecimals(address) {
  if (!decimals[address]) {
    const contract = await ethInstance.getReadContractByAddress(ChainLinkAggregator, address);

    decimals[address] = await contract.decimals();
  }

  return decimals[address];
}

export async function getOracle(tournamentId) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const o = await contract.getOracle(tournamentId);

  return {
    challengeDepositCostTokens: o[0],
    challengeToken: o[1],
    currentTickerSymbols: o[2],
    currentPriceFeeds: o[3]
  };
}

export async function getTokenListRound(
  tournamentId = process.env.REACT_APP_TOURNAMENT_ID,
  roundId
) {
  if (!tokenLists[tournamentId]) {
    tokenLists[tournamentId] = {};
  }

  if (!tokenLists[tournamentId][roundId]) {
    tokenLists[tournamentId][roundId] = [];

    const round = await getRoundDetails(tournamentId, roundId);

    let tickerSymbols = round.tickerSymbols;
    let priceFeeds = round.priceFeeds;

    if (tickerSymbols.length === 0) {
      const oracle = await getOracle(tournamentId);
      tickerSymbols = oracle.currentTickerSymbols;
      priceFeeds = oracle.currentPriceFeeds;
    }

    const decimals = await Promise.all(priceFeeds.map((feed) => getDecimals(feed)));

    for (let i = 0; i < tickerSymbols.length; i++) {
      // eslint-disable-next-line no-control-regex
      const sym = Buffer.from(tickerSymbols[i].slice(2), 'hex')
        .toString()
        // eslint-disable-next-line no-control-regex
        .replace(/\x00/g, '');

      tokenLists[tournamentId][roundId].push({
        tickerSymbol: sym,
        name: sym,
        priceFeed: priceFeeds[i],
        decimals: decimals[i]
      });
    }
  }

  return tokenLists[tournamentId][roundId];
}

export async function getTokenBalance() {
  const account = await ethInstance.getEthAccount();

  const contract = await ethInstance.getContract('read', Token);

  const balance = await contract.balanceOf(account);

  // console.log('account', account, 'contract', contract, 'balance', balance.toString());

  return fromWei(balance);
}

export async function getVotesByVoterID(
  voterId,
  tournamentId = process.env.REACT_APP_TOURNAMENT_ID
) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);
  const roundIds = await contract.getVotesByVoterID(voterId, tournamentId);

  const d = {};

  for (const roundId of roundIds) {
    const [roundDetails, rewards, vote] = await Promise.all([
      getRoundDetails(tournamentId, roundId),
      getRoundRewards(voterId, tournamentId, roundId),
      contract.getVote(tournamentId, roundId, voterId)
    ]);

    const winnings = parseFloat(fromWei(rewards));

    d[roundId] = {
      winnerIndex: roundDetails.finalWinnerIndex,
      winnings,
      roundId,
      metadata: vote[0],
      choices: vote[1],
      weights: vote[2]
    };
  }

  return d;
}

export async function identityAlreadyVoted(voterId, tournamentId) {
  const trollbox = await ethInstance.getContract('read', PermissionlessMarkets);
  const currentRound = await trollbox.getCurrentRoundId(tournamentId);

  const vote = await trollbox.getVote(tournamentId, currentRound, voterId);
  //  console.log('identityAlreadyVoted', { vote, voterId, tournamentId, currentRound });

  if (vote[1].length === 0 && vote[2].length === 0) {
    return false;
  }

  return vote;
}

export async function getRoundResolved(tournamentId, roundId) {
  const winner = await getWinner(tournamentId, roundId);

  return winner > 0;
}

export async function getWinner(tournamentId, roundId) {
  let winner;

  try {
    const markets = await ethInstance.getContract('read', PermissionlessMarkets);

    const round = await markets.getRoundDetails(tournamentId, roundId);

    winner = round[1];
  } catch (err) {
    console.error(err);
  }

  return winner;
}

export async function getRoundDetails(tournamentId, roundId, refresh = false) {
  if (!rounds[tournamentId]) {
    rounds[tournamentId] = {};
  }

  if (!rounds[tournamentId][roundId] || refresh) {
    const markets = await ethInstance.getContract('read', PermissionlessMarkets);

    const deets = await markets.getRoundDetails(tournamentId, roundId);

    rounds[tournamentId][roundId] = {
      roundId,
      finalWinnerIndex: deets[0],
      proposedWinnerIndex: deets[1],
      challengeWinnerIndex: deets[2],
      challenger: deets[3],
      challengeEvidence: deets[4],
      confimationEvidence: deets[5],
      startingPrices: deets[6],
      proposedFinalPrices: deets[7],
      finalPrices: deets[8],
      tickerSymbols: deets[9],
      priceFeeds: deets[10]
    };
  }

  return rounds[tournamentId][roundId];
}

export async function getIdentitiesForAccount(tournamentId = process.env.REACT_APP_TOURNAMENT_ID) {
  // console.log('getIdentitiesForAccount');
  const [contract, account] = await Promise.all([
    ethInstance.getContract('read', VoterID),
    ethInstance.getEthAccount()
  ]);
  const numIdentities = await contract.balanceOf(account);
  const tokenPromises = [];

  const identities = {};
  for (let i = 0; i < numIdentities; i++) {
    tokenPromises.push(contract.tokenOfOwnerByIndex(account, i));
  }
  const tokenIds = await Promise.all(tokenPromises);

  for (const t of tokenIds) {
    const tokenId = parseInt(t).toString();

    const [unclaimedTokens, currentV] = await Promise.all([
      getUnclaimedTokens(tokenId, tournamentId),
      getVoiceCredits(tournamentId)
    ]);

    const obj = {
      unclaimedTokens,
      tokenId,
      fvtBalance: unclaimedTokens,
      voteMarkets: [
        {
          name: 'Macro',
          v: currentV,
          rewards: unclaimedTokens
        }
      ]
    };
    identities[tokenId] = obj;
    // obj.unclaimedTokens = unclaimedTokens;
    // obj.tokenId = tokenId;
    // // console.log('obj.unclaimedTokens', obj.unclaimedTokens);
    // obj.fvtBalance = unclaimedTokens;
    // obj.voteMarkets = [
    //   {
    //     name: 'Macro',
    //     v: currentV,
    //     rewards: unclaimedTokens
    //   }
    // ];
  }

  return identities;
}

async function getLivePrices(tournamentId = process.env.REACT_APP_TOURNAMENT_ID) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const usdPrices = await contract.getLivePrices(tournamentId);

  return usdPrices;
}

export async function getPricesForRound(tournamentId, roundId, tokens) {
  const prices = {};
  let finalPrices, startingPrices;

  const state = await getRoundState(tournamentId, roundId);

  //if voting state and current round is not first get final prices as startPrices from prev roundId
  const round = await getRoundDetails(
    tournamentId,
    state === ROUND_STATE.VOTING && roundId >= 2 ? roundId - 1 : roundId
  );

  switch (state) {
    case ROUND_STATE.VOTING:
      startingPrices = round.finalPrices;
      finalPrices = await getLivePrices(tournamentId);
      break;
    case ROUND_STATE.MARKET:
      startingPrices = round.startingPrices;
      finalPrices = await getLivePrices(tournamentId);
      break;
    case ROUND_STATE.CHALLENGE:
      startingPrices = round.startingPrices;
      finalPrices = round.proposedFinalPrices;
      break;
    default:
      startingPrices = round.startingPrices;
      finalPrices = round.finalPrices;
      break;
  }

  for (let i = 0; i < tokens.length; i++) {
    const token = tokens[i];
    const finalPrice = finalPrices[i] / 10 ** token.decimals;
    const startingPrice = startingPrices[i] / 10 ** token.decimals;

    prices[token.tickerSymbol] = {
      finalPrice,
      startingPrice,
      difference: ((finalPrice - startingPrice) * 100) / startingPrice
    };
  }

  return prices;
}

export async function getProposedPrices(tournamentId, roundId) {
  const round = await getRoundDetails(tournamentId, roundId);

  return round.proposedFinalPrices;
}

export async function getFinalPrices(tournamentId, roundId) {
  const round = await getRoundDetails(tournamentId, roundId);

  return round.finalPrices;
}

export async function isVotingPeriod(tournamentId) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const state = await contract.getCurrentRoundState(tournamentId);

  return parseInt(state) === 1;
}

export async function getCurrentRoundState(tournamentId) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const state = await contract.getCurrentRoundState(tournamentId);

  return parseInt(state);
}

export async function getRoundState(tournamentId, roundId) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const state = await contract.getRoundState(tournamentId, roundId);
  return parseInt(state);
}

export async function getLastRoundReward(tournamentId, voterId) {
  const trollbox = await ethInstance.getContract('read', PermissionlessMarkets);

  const lastRoundReward = await trollbox.getLastRoundReward(tournamentId, voterId);

  return parseInt(lastRoundReward);
}

export async function getNumIdentities() {
  const identity = await ethInstance.getContract('read', VoterID);

  const numIdentities = await identity.numIdentities();

  return parseInt(numIdentities);
}

export async function roundAlreadyResolved(tournamentId, roundId) {
  const winnerIndex = await getWinnerIndex(tournamentId, roundId);

  return winnerIndex > 0;
}

export async function getRoundResolution(tournamentId) {
  const currentRoundId = await getCurrentRoundId(tournamentId);

  const resolution = {};

  for (let roundId = 1; roundId < currentRoundId - 1; roundId++) {
    resolution[roundId] = await roundAlreadyResolved(tournamentId, roundId);
  }

  return resolution;
}

export async function getRoundRewards(voterId, tournamentId, roundId) {
  if (!voterId) {
    return null;
  }

  const trollbox = await ethInstance.getContract('read', PermissionlessMarkets);

  const bonus = await trollbox.getRoundBonus(voterId, tournamentId, roundId);

  return bonus;
}

export async function getVoiceCredits(tournamentId) {
  const tournament = await getTournament(tournamentId);

  return tournament.voiceUBI;
}

export async function getOwner(token) {
  const id = await ethInstance.getContract('read', VoterID);

  return await id.ownerOf(token);
}

export async function getUnclaimedTokens(
  voterId,
  tournamentId = process.env.REACT_APP_TOURNAMENT_ID
) {
  const trollbox = await ethInstance.getContract('read', PermissionlessMarkets);

  const tokensWon = await trollbox.getTokensWon(tournamentId, voterId);

  // console.log('tokensWon', voterId, tokensWon, fromWei(tokensWon));

  return parseFloat(fromWei(tokensWon));
}

export async function getTokenSymbol(tokenAddress) {
  if (!symbols[tokenAddress]) {
    const contract = await ethInstance.getReadContractByAddress(Token, tokenAddress);
    const symbol = await contract.symbol();
    symbols[tokenAddress] = symbol;
    return symbol;
  }

  return symbols[tokenAddress];
}

export async function getTournament(tournamentId) {
  if (!tournaments[tournamentId]) {
    const contract = await ethInstance.getContract('read', PermissionlessMarkets);

    const [currentRoundId, addrs, t, tournamentAddresses] = await Promise.all([
      contract.getCurrentRoundId(tournamentId),
      contract.getTournamentAddresses(tournamentId),
      contract.getTournament(tournamentId),
      contract.getTournamentAddresses(tournamentId)
    ]);

    const tournament = {
      tournamentId,
      startTime: parseInt(t[1]),
      votingPeriodLengthSeconds: parseInt(t[2]),
      marketPeriodLengthSeconds: parseInt(t[3]),
      challengePeriodLengthSeconds: parseInt(t[4]),
      roundRewardTokens: fromWei(t[5]),
      voteCostTokens: fromWei(t[6]),
      rewardFunding: fromWei(t[7]),
      rewardsOwed: fromWei(t[8]),
      voiceUBI: parseInt(t[9]),
      roundToConfirm: t[10]
    };

    const rewardAddress = tournamentAddresses[1];
    tournament.rewardTokenSymbol = await getTokenSymbol(rewardAddress);

    tournament.currentRoundId = parseInt(currentRoundId);
    tournament.roundLengthSeconds = parseInt(t[2]) + parseInt(t[3]) + parseInt(t[4]);
    tournaments[tournamentId] = tournament;

    tournament.voteToken = addrs[0];
    tournament.rewardToken = addrs[1];
    tournament.identity = addrs[2];
    tournament.owner = addrs[3];
    tournament.taxMan = addrs[4];
  }

  return tournaments[tournamentId];
}

export async function getVoteCost(tournamentId) {
  const t = await getTournament(tournamentId);

  return t.voteCostTokens;
}

export async function getVoteTotals(tournamentId, roundId, numOptions) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const deferred = [];

  for (let i = 1; i <= numOptions; i++) {
    deferred.push(contract.getVoteTotals(tournamentId, roundId, i));
  }

  const votes = await Promise.all(deferred);

  return votes.map((x) => parseInt(x));
}

export async function getVoteTotal(tournamentId, roundId, option) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const totals = await contract.getVoteTotals(tournamentId, roundId, option);

  return totals.map((x) => parseInt(x));
}

export async function getCurrentRoundId(tournamentId) {
  const contract = await ethInstance.getContract('read', PermissionlessMarkets);

  const roundId = await contract.getCurrentRoundId(tournamentId);

  return parseInt(roundId);
}

export function getDaysBack(tournament, roundId) {
  if (roundId === 0 || roundId === tournament.currentRoundId - 1) {
    // console.log({ tournament });

    const roundStart =
      tournament.startTime + (tournament.currentRoundId - 1) * tournament.roundLengthSeconds;

    const now = Math.floor(new Date().getTime() / 1000);

    return Math.floor((now - roundStart) / 86400);
  } else {
    return 7;
  }
}

export async function getTokenAddress(chainId) {
  // console.log('Token', chainId, Token.networks);

  return Token.networks[chainId].address;
}

export async function getMarketsAddress(chainId) {
  // console.log('PermissionlessMarkets', chainId, PermissionlessMarkets.networks);

  return PermissionlessMarkets.networks[chainId].address;
}

export async function getWinnerIndex(tournamentId, roundId) {
  const round = await getRoundDetails(tournamentId, roundId);

  return parseInt(round.finalWinnerIndex);
}

export async function getApprovalForVoteToken(tournamentId, account) {
  const tournament = await getTournament(tournamentId);

  const contract = await ethInstance.getReadContractByAddress(Token, tournament.voteToken);

  const marketsAddress = await getMarketsAddress(process.env.REACT_APP_DEFAULT_CHAIN_ID ?? '137');
  //  const marketsAddress = await getMarketsAddress('333') // TODO: make dynamic

  const approval = await contract.allowance(account, marketsAddress);

  if (tournament.voteCostTokens > approval) {
    const writeContract = await ethInstance.getWriteContractByAddress(Token, tournament.voteToken);

    await writeContract.approve(marketsAddress, '999999999999999999999999999999999999', {
      from: account
    });
  }

  return toWei(tournament.voteCostTokens);
}

export async function getClosedBallotForRound(tournament, roundId, chainId) {
  const { tournamentId } = tournament;

  const query = `
      query getRound($tournamentId: BigInt! $roundId: BigInt!) {
      tournaments(where: {tournamentId_eq: $tournamentId}) {
        tournamentId
        rounds(where: {roundId_eq: $roundId}) {
          roundId
          priceFeeds
          finalWinnerIndex
          finalPrices
          tickerSymbols
          voteTotals
          startingPrices
        }
      }
    }`;

  const variables = {
    tournamentId,
    roundId
  };

  const fetchData = await fetch(process.env.REACT_APP_GRAPH_QL_ENDPOINT, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query,
      variables
    })
  });

  const graphResponse = await fetchData.json();
  const { data } = graphResponse;

  if (!data.tournaments.length) return;

  const { rounds } = data.tournaments[0];
  const round = rounds[0];

  const voteSum = round.voteTotals.reduce((x, y) => x + y, 0);
  const voteMax = Math.max(...round.voteTotals);

  const prices = {};
  const tokenList = [];

  const calls = [];

  const decimalsInterface = new ethers.Interface(ChainLinkAggregator.abi);

  for (let index = 0; index < round.priceFeeds.length; index++) {
    const contractAddress = round.priceFeeds[index];

    const newCall = {
      contractAddress,
      functionName: 'decimals',
      args: [],
      contractInterface: decimalsInterface
    };

    calls.push(newCall);
  }

  const decimals = await MulticallRequest(chainId, calls);

  for (let i = 0; i < round.tickerSymbols.length; i++) {
    const token = round.tickerSymbols[i];
    const finalPrice = round.finalPrices[i] / 10 ** decimals[i];
    const startingPrice = round.startingPrices[i] / 10 ** decimals[i];

    // eslint-disable-next-line no-control-regex
    const sym = Buffer.from(token.slice(2), 'hex')
      .toString()
      // eslint-disable-next-line no-control-regex
      .replace(/\x00/g, '');

    prices[sym] = {
      finalPrice,
      startingPrice,
      difference: ((finalPrice - startingPrice) * 100) / startingPrice
    };

    tokenList.push({
      tickerSymbol: sym,
      name: sym,
      priceFeed: round.priceFeeds[i],
      decimals: decimals[i]
    });
  }

  const votes = round.voteTotals.map((vote, index) => {
    return {
      weight: vote,
      icon: `assets/icons/${tokenList[index].tickerSymbol.toLowerCase()}.png`,
      name: tokenList[index].name,
      tickerSymbol: tokenList[index].tickerSymbol,
      daysBack: '',
      price: {
        symbol: '$',
        value: prices[tokenList[index].tickerSymbol].finalPrice,
        difference: prices[tokenList[index].tickerSymbol].difference
      }
    };
  });

  const projectedWinnerIndex = round.voteTotals.findIndex((x) => x === voteMax);

  const projectedWinner =
    projectedWinnerIndex > -1 ? tokenList[projectedWinnerIndex] : tokenList[0];

  const marketWinner = tokenList[round.finalWinnerIndex - 1];

  const startDate = tournament.startTime + (roundId - 1) * tournament.roundLengthSeconds;

  return {
    number: roundId,
    startDate,
    endDate: startDate + tournament.roundLengthSeconds,
    resultsDate: startDate + tournament.roundLengthSeconds,
    projectedWinner: projectedWinner && {
      icon: `assets/icons/${projectedWinner.tickerSymbol.toLowerCase()}.png`,
      name: projectedWinner?.name
    },
    marketWinner: marketWinner && {
      icon: `assets/icons/${marketWinner?.tickerSymbol.toLowerCase()}.png`,
      name: marketWinner?.name
    },
    votes,
    voteSum,
    voteMax
  };
}

export async function getBallotForRound(tournament, roundId) {
  const { tournamentId } = tournament;

  const tokenList = await getTokenListRound(tournamentId, roundId);

  const [winnerIndex, prices, totals] = await Promise.all([
    getWinnerIndex(tournamentId, roundId),
    getPricesForRound(tournamentId, roundId, tokenList),
    getVoteTotals(tournamentId, roundId, tokenList.length)
  ]);

  const voteSum = totals.reduce((x, y) => x + y, 0);
  const voteMax = Math.max(...totals);

  const votes = totals.map((vote, index) => {
    return {
      weight: vote,
      icon: `assets/icons/${tokenList[index].tickerSymbol.toLowerCase()}.png`,
      name: tokenList[index].name,
      tickerSymbol: tokenList[index].tickerSymbol,
      daysBack: '', // todo: what are we going to do with this?
      price: {
        symbol: '$',
        value: prices[tokenList[index].tickerSymbol].finalPrice,
        difference: prices[tokenList[index].tickerSymbol].difference
      }
    };
  });

  const projectedWinnerIndex = totals.findIndex((x) => x === voteMax);

  const projectedWinner =
    projectedWinnerIndex > -1 ? tokenList[projectedWinnerIndex] : tokenList[0];

  const marketWinner = tokenList[winnerIndex - 1];

  const startDate = tournament.startTime + (roundId - 1) * tournament.roundLengthSeconds;

  return {
    number: roundId,
    startDate,
    endDate: startDate + tournament.roundLengthSeconds,
    resultsDate: startDate + tournament.roundLengthSeconds,
    projectedWinner: projectedWinner && {
      icon: `assets/icons/${projectedWinner.tickerSymbol.toLowerCase()}.png`,
      name: projectedWinner?.name
    },
    marketWinner: marketWinner && {
      icon: `assets/icons/${marketWinner?.tickerSymbol.toLowerCase()}.png`,
      name: marketWinner?.name
    },
    votes,
    voteSum,
    voteMax
  };
}
