import { MemoryRouter } from 'react-router-dom'

export function withRouter (component, { route = '/' } = {}) {
  window.history.pushState({}, 'Test page', route)

  return (
    <MemoryRouter>
      {component}
    </MemoryRouter>
  )
}
