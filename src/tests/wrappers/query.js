import {
  setLogger,
  QueryClient,
  QueryClientProvider
} from 'react-query'

setLogger({
  log: console.log,
  warn: console.warn,
  error: () => {}
})

export function withQuery (component) {
  const queryClient = new QueryClient({
    defaultOptoins: {
      queries: {
        retry: false
      }
    }
  })

  return (
    <QueryClientProvider client={queryClient}>
      {component}
    </QueryClientProvider>
  )
}
